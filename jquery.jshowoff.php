<?php
/*
Plugin Name: jquery.jshowoff support
Description: Add support for jquery.jshowoff, a slideshow framework.
Author: Dave Mainville
Author URI: http://superdave2u.com
*/
if(!class_exists('jqueryJshowoff')) {
	class jqueryJshowoff {
		function __construct() {
			add_action('init',array($this,'registerScripts'));
		}
		function registerScripts() {
			wp_enqueue_script('jquery');
			wp_enqueue_script( 
				'jquery.jshowoff', 
				plugins_url( 'js/jquery.jshowoff.min.js' , __FILE__ ), 
				'jquery'
			);
		}
	}
	$jqueryJshowoff = new jqueryJshowoff();
}

?>